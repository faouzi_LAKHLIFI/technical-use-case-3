/**
 * Created by faouziLKF on 20/03/2022.
 */

trigger CaseTrigger on Case (before insert, before update/*, after update*/) {
    new CaseTriggerHandler().run();
}