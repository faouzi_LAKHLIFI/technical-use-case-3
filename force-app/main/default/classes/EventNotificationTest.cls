/**
 * Created by faouziLKF on 21/03/2022.
 */

@isTest
public with sharing class EventNotificationTest {

    @isTest
    public static void updateCasesToNotifyContacts(){
        Boolean isError = false;
        Account a1 = (Account)TestDataFactory.createSObject('Account',
                new Map<String,Object>{
                        'Name' => 'Test Account 1'
                },true);
        Contact c1 = (Contact)TestDataFactory.createSObject('Contact',
                new Map<String,Object>{
                        'AccountId' => a1.Id,
                        'Email' => '1d525519ce@catdogmail.live',//temporary email for testing purposes
                        'FirstName' => 'Faouzi',
                        'LastName' => 'LAKHLIFI'
                },true);

        List<Case> cc1 = TestDataFactory.createSObjectList('Case',
                new Map<String,Object>{
                        'AccountId' => a1.Id,
                        'Status' => 'New',
                        'Description' => 'Test Case 1'
                },15);


        Account a2 = (Account)TestDataFactory.createSObject('Account',
                new Map<String,Object>{
                        'Name' => 'Test Account 2'
                },true);
        Contact c2 = (Contact)TestDataFactory.createSObject('Contact',
                new Map<String,Object>{
                        'AccountId' => a2.Id,
                        'Email' => '1d525519ce@catdogmail.live',//temporary email for testing purposes
                        'FirstName' => 'Faouzi',
                        'LastName' => 'LAKHLIFI'
                },
                true);

        List<Case> cc2 = TestDataFactory.createSObjectList('Case',
                new Map<String,Object>{
                    'AccountId' => a2.Id,
                    'Status' => 'New',
                    'Description' => 'Test Case 2'
                },
                15, true);

        List<Case> allCases = new List<Case>();
        allCases.addAll(cc1);
        allCases.addAll(cc2);

        Test.startTest();

        for(Case c : allCases){
            c.Status = 'Closed';
        }

        try{
            List<Database.SaveResult> results = Database.update(allCases);
        }
        catch (Exception ex){
            System.debug(ex.getMessage());
            isError = true;
        }

        Test.stopTest();

        System.assertEquals(isError,false);
    }

    // I Test the notifyAttendeesByEmail with a bug number of records using the TestDataFactory framework
    @isTest
    public static void massNotifyAttendeesByEmail(){
        Boolean isError = false;
        List<Contact> contacts = TestDataFactory.createSObjectList('Contact',
                new Map<String,Object>{
                        'Email' => '1d525519ce@catdogmail.live',//temporary email for testing purposes
                        'FirstName' => 'Faouzi',
                        'LastName' => 'LAKHLIFI'
                },15);


        Test.startTest();
        try{
            ExternalSystemService.notifyAttendeesByEmail(contacts);
        }
        catch (Exception ex){
            System.debug(ex.getMessage());
            isError = true;
        }

        Test.stopTest();

        System.assertEquals(isError,false);
    }

}