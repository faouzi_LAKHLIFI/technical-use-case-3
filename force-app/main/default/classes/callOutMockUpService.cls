/**
 * Created by flakhlifi on 22/3/2022.
 */

@isTest
global class callOutMockUpService implements HttpCalloutMock{
    global static HTTPResponse respond(HTTPRequest req) {
        HTTPResponse resp = new HTTPResponse();
        resp.setStatusCode(201);
        return resp;
    }
}