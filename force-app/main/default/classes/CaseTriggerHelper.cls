/**
 * Created by faouziLKF on 19/03/2022.
 */

public with sharing class CaseTriggerHelper{
    public static void checkReservation(List<Case> cases){

        //Best Practice #2 - Bulkify the SQL calls to the database :
        //// in this method query the database for accounts/account-contacts only once, although we iterate over the contacts list twice but it's
        //// more scalable than querying accounts inside the for loop

        List<ID> accIds = new List<Id>();

        for(Case r : cases){
            accIds.add(r.AccountId);
        }

        Map<ID, Account> accounts = new Map<ID, Account>([SELECT Id, Name, (SELECT Id FROM Contacts) FROM Account WHERE Id in :accIds]);

        for(Case r : cases){
            if(r != null && r.AccountId != null){
                Account account = accounts.get(r.AccountId);
                if(account != null){
                    Integer s = account.Contacts.size();
                    if(s == 0){
                        r.addError('You cannot create a request for accounts without contacts');
                    }
                    else {
                        switch on r.Origin {
                            when 'Web' {
                                if(s >= 2 ){
                                    r.addError('Web request are only allowed to have one attendee');
                                }
                            }
                            when 'Phone'{
                                if(s >= 4 ){
                                    r.addError('Phone request are only allowed to have three attendee');
                                }
                            }
                        }
                    }
                }
                else{
                    r.addError('Invalid account');
                }

            }else {
                r.addError('You cannot create a request without attaching an account');
            }
        }

    }

    public static void notifyForReservation(List<Case> updatedCases, Map<Id, Case> oldMap){
        List<Id> accountIds = new List<Id>();
        for(Case updatedCase : updatedCases){
            Case OldCase = oldMap.get(updatedCase.Id);
            System.debug(updatedCase);
            System.debug(OldCase);
            // this way we do not resend the
            if(OldCase.Status != 'Closed' && updatedCase.Status == 'Closed'){
                accountIds.add(updatedCase.AccountId);
            }
        }
        if(accountIds.size() > 0) ExternalSystemService.registerAttendees(accountIds);
    }
}