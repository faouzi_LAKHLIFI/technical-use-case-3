public  class ExternalSystemService {


    // Best Practice #8: Use @future Appropriately
    // - the @future method should be invoked with a batch of records so that it is only invoked once for all records it needs to process
    @future (callout=true)
    public static void registerAttendees(List<Id> AccountIds){
        //Best Practice #2 - removing DLM queries from inside loops
        //in this case we query all contacts info at once

        //Best Practice : using named credentials instead of a hard coded url;

        //Best Practice : logging errors to a custom object using a dedicated framework

        List<Contact> attendees = [SELECT id,name, email FROM Contact WHERE AccountId in :AccountIds];
        List<Contact> contactsToMail = new List<Contact>();
        Http http = new Http();
        HttpRequest request = new HttpRequest();
        request.setEndpoint('callout:ReservationExternalSystem');
        request.setMethod('POST');
        request.setHeader('Content-Type', 'application/json;charset=UTF-8');
        // Set the body as a JSON object

        HttpResponse response;

        for(Contact c : attendees){
            request.setBody('{"name":"'+ c.Email + '-' + c.Name  +'}');
            if(Test.isRunningTest()) {
                response = callOutMockUpService.respond(request);
            }
            else{
                response = http.send(request);
            }
            // Parse the JSON response
            if (response.getStatusCode() != 201) {
                System.debug('The status code returned was not expected: ' +response.getStatusCode() + ' ' + response.getStatus());
                Log.error('The status code returned was not expected: ' +response.getStatusCode() + ' ' + response.getStatus());
            }
            else {
                // Everything went as expected.
                contactsToMail.add(c);
            }
        }
        notifyAttendeesByEmail(contactsToMail);
    }

    public static void notifyAttendeesByEmail(List<Contact> contacts){

        List<Messaging.SingleEmailMessage> lstMails = new List<Messaging.SingleEmailMessage>();
        for(Contact c : contacts){
            Messaging.SingleEmailMessage message = new Messaging.SingleEmailMessage();
            message.toAddresses = new String[] { c.Email };
            message.optOutPolicy = 'FILTER';
            message.subject = 'Hello ' + c.Name +' The is Event Booked';
            message.plainTextBody = 'Event Booked';
            lstMails.add(message);
        }

        Messaging.SendEmailResult[] results = Messaging.sendEmail(lstMails);

        for(Messaging.SendEmailResult result : results){
            if (results[0].success) {
                System.debug('The email was sent successfully.');
            } else {
                System.debug('The email failed to send: ' + results[0].errors[0].message);
                Log.error('The email failed to send: ' + results[0].errors[0].message);
            }
        }
    }

}