/**
 * Created by faouziLKF on 19/03/2022.
 */

public with sharing class CaseTriggerHandler extends TriggerHandler{
        private List<Case> oldList, newList;
        private Map<Id, Case> oldMap, newMap;

        public CaseTriggerHandler() {
        oldList = (List<Case>) Trigger.old;
        newList = (List<Case>) Trigger.new;
        oldMap = (Map<Id, Case>) Trigger.oldMap;
        newMap = (Map<Id, Case>) Trigger.newMap;
    }

    protected override void beforeInsert() {
        //Best Practice #1 Bulkify trigger methods
        CaseTriggerHelper.checkReservation(newList);
    }
//    protected override void afterUpdate() {
//        //Best Practice #1 Bulkify trigger methods
//        CaseTriggerHelper.notifyForReservation(newList, oldMap);
//    }
    protected override void beforeUpdate() {
        //Best Practice #1 Bulkify trigger methods
        CaseTriggerHelper.notifyForReservation(newList, oldMap);
    }
}